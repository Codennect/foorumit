var express = require('express');
var router = express.Router();

var authentication = require('../utils/authentication');
var Models = require('../models');

// Huom! Kaikki polut alkavat polulla /messages

// GET /messages/:id
router.get('/:id', function(req, res, next) {
  // Hae viesti tällä id:llä ja siihen liittyvät vastaukset tässä (Vinkki: findOne ja sopiva include)
  var messageId = req.params.id;
  Models.Message.findOne( {
      where: {id: messageId },
      include: { model: Models.Reply }
  }).then(function(message) {
    res.json(message);
  });
});

// POST /messages/:id/reply
router.post('/:id/reply', function(req, res, next){
  // Lisää tällä id:llä varustettuun viestiin...
  var messageId = req.params.id;
// Palauta lisätty vastaus
  Models.Reply.create({
    content: req.body.content,
    MessageId: req.body.MessageId
  }).then(function(topic) {
    res.json(topic)
  })
});

// POST /messages/:topicID/messageTopic/message
router.post('/:topicId/:messageTopic/:message' , function(req,res,next) {
  var topicId = req.params.topicId;
  var messageTopic = req.params.messageTopic;
  var messageToAdd = req.params.message;
  Models.Message.create({
    title: messageTopic,
    content: messageToAdd,
    TopicId: topicId
  }).then(function(message){
    res.json(message)
  })
});

module.exports = router;
