FoorumApp.controller('TopicsListController', function($scope, $location, Api){
  
  $scope.newTopic = {
        name: '',
        description: ''
    }

  
  Api.getTopics().success(function(topics){
    $scope.topics = topics;
    
    });

   $scope.addTopic = function(topic) {
        Api.addTopic(topic).success(function(topic){
         $location.path('topics/'+topic.id)
        })
        .error(function(data, status, headers, config){
          console.log('Jotain meni pieleen...');
        })
    }


});
