FoorumApp.controller('ShowTopicController', function($scope, $rootScope, $routeParams, $location, Api){
    Api.getTopic($routeParams.id).success(function(topic){
    $scope.topic = topic;
    });
    
    $scope.postMessage = function(newTitle, newMessage) {
      Api.addMessage($routeParams.id, {
        title: newTitle,
        content: newMessage,
        TopicId: $routeParams.id
      }).success(function(message) {
        //$scope.topic.Messages.push(message);
        $location.path('/messages/' + message.id )
      })
    }
    
});
